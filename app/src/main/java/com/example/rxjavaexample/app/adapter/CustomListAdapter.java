package com.example.rxjavaexample.app.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.example.rxjavaexample.app.R;
import com.example.rxjavaexample.app.model.Track;

import javax.inject.Inject;
import java.util.List;

public class CustomListAdapter extends BaseAdapter {

    @Inject
    LayoutInflater mLayoutInflater;

    private List<Track> mElements;

    @Inject
    public CustomListAdapter() {
    }

    @Override
    public int getCount() {
        return mElements == null ? 0 : mElements.size();
    }

    @Override
    public Object getItem(int position) {
        return mElements.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mElements.get(position).name.hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.list_item, parent, false);
            assert convertView != null;

            TextView view = (TextView) convertView.findViewById(R.id.text1);
            convertView.setTag(R.id.text1, view);
        }

        TextView textView = (TextView) convertView.getTag(R.id.text1);

        Track element = mElements.get(position);
        assert textView != null;

        textView.setText(element.name);

        return convertView;
    }

    public void swapElements(List<Track> elements) {
        mElements = elements;
        notifyDataSetChanged();
    }
}
