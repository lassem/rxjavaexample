package com.example.rxjavaexample.app;

import com.example.rxjavaexample.app.api.SpotifyService;
import com.example.rxjavaexample.app.model.Track;
import com.example.rxjavaexample.app.model.Tracks;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import java.util.List;

import retrofit.RestAdapter;
import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.functions.Func2;
import rx.functions.Func3;
import rx.schedulers.Schedulers;
import rx.subjects.BehaviorSubject;
import rx.subjects.PublishSubject;
import rx.subjects.ReplaySubject;

public class TracksProvider {

    private static final String BASE_URL = "http://ws.spotify.com/";

    private PublishSubject<Void> mDemandDownloadsSubject;
    private ReplaySubject<Track> mAddTrackSubject;
    private ReplaySubject<Track> mRemoveTrackSubject;
    private BehaviorSubject<List<Track>> mMergedTracksSubject;

    private final SpotifyService mService;

    public TracksProvider() {
        final RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL)
                .build();

        final List<Track> defTracks = Lists.newArrayList();

        mService = restAdapter.create(SpotifyService.class);
        mDemandDownloadsSubject = PublishSubject.create();
        mMergedTracksSubject = BehaviorSubject.create(defTracks);
        mAddTrackSubject = ReplaySubject.create();
        mRemoveTrackSubject = ReplaySubject.create();

        final BehaviorSubject<Integer> requestsCompleted = BehaviorSubject.create(0);
        final ReplaySubject<List<Track>> requestsSubject = ReplaySubject.create();
        final BehaviorSubject<List<Track>> groupedTracksSubject = BehaviorSubject.create(defTracks);

        requestsCompleted
                .sample(mDemandDownloadsSubject)
                .flatMap(page -> mService
                        .listTracks("a", page)
                        .map(tracks -> tracks.tracks))
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(requestsSubject);

        requestsSubject
                .scan(0, (integer, tracks) -> integer + 1)
                .subscribe(requestsCompleted);

        requestsSubject
                .scan(Lists.<Track>newArrayList(), (tracks, accumulated) -> {
                    tracks.addAll(accumulated);
                    return tracks;
                })
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(groupedTracksSubject);

        final Observable<List<Track>> addReduced = mAddTrackSubject
                .scan((List<Track>)Lists.<Track>newArrayList(), (tracks, track) -> {
                    tracks.add(track);
                    return tracks;
                })
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io());

        final Observable<List<Track>> removeReduced = mRemoveTrackSubject
                .scan((List<Track>)Lists.<Track>newArrayList(), (tracks, track) -> {
                    tracks.add(track);
                    return tracks;
                })
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io());

        Observable
                .combineLatest(addReduced, removeReduced, groupedTracksSubject, (added, removed, grouped) -> {
                    final List<Track> tracks = Lists.newArrayList(Iterables.concat(added, grouped));
                    return Lists.newArrayList(Iterables.filter(tracks, input -> !removed.contains(input)));
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mMergedTracksSubject);
    }

    public void addTrack(Track track) {
        mAddTrackSubject.onNext(track);
    }

    public void deleteTrack(Track track) {
        mRemoveTrackSubject.onNext(track);
    }

    public void downloadTracks() {
        mDemandDownloadsSubject.onNext(null);
    }

    public Subscription subscribeTo(Observer<List<Track>> observer) {
        return mMergedTracksSubject.subscribe(observer);
    }
}
