package com.example.rxjavaexample.app;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rxjavaexample.app.adapter.CustomListAdapter;
import com.example.rxjavaexample.app.dagger.ActivityModule;
import com.example.rxjavaexample.app.model.Track;

import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import rx.Observer;
import rx.Subscription;

public class MainActivity extends Activity implements AbsListView.OnScrollListener {

    private static final String LOG_TAG = MainActivity.class.getCanonicalName();
    @InjectView(android.R.id.text1)
    TextView mText;
    @InjectView(android.R.id.button1)
    TextView mButton;
    @InjectView(android.R.id.list)
    ListView mList;

    @Inject
    CustomListAdapter mListAdapter;
    @Inject
    TracksProvider mElementsProvider;

    private Subscription mListSubscription;

    private Observer<List<Track>> mListObserver = new Observer<List<Track>>() {

        @Override
        public void onCompleted() {
        }

        @Override
        public void onError(Throwable e) {
            Log.e(LOG_TAG, "error", e);
            Toast.makeText(MainActivity.this, "Error downloading data", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onNext(List<Track> elements) {
            mListAdapter.swapElements(elements);
            mText.setText("Count : " + elements.size());
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);

        App.get(this).getApplicationGraph().plus(new ActivityModule(this)).inject(this);

        mList.setAdapter(mListAdapter);
        mList.setOnScrollListener(this);

        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final Track track = (Track) parent.getItemAtPosition(position);
                mElementsProvider.deleteTrack(track);
            }
        });

        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Track track = new Track();
                track.name = "dodany";
                mElementsProvider.addTrack(track);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mListSubscription = mElementsProvider.subscribeTo(mListObserver);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mListSubscription.unsubscribe();
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (firstVisibleItem + visibleItemCount == totalItemCount) {
            mElementsProvider.downloadTracks();
        }
    }
}
