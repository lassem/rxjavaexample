package com.example.rxjavaexample.app.dagger;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;

import com.example.rxjavaexample.app.MainActivity;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(
        addsTo = AppModule.class,
        injects = {
                MainActivity.class
        }, library = true
)
public class ActivityModule {

    private final Activity mActivity;

    public ActivityModule(Activity activity) {
        mActivity = activity;
    }

    @Provides
    @Singleton
    @ForActivity
    public Context activityContext() {
        return mActivity;
    }

    @Provides
    @Singleton
    public LayoutInflater provideLayoutInflater(@ForActivity Context context) {
        return LayoutInflater.from(context);
    }
}
